%global baserelease 1
%{?!_pkgdocdir:%global _pkgdocdir %{_docdir}/%{name}-%{version}}
%global nodejs_epoch 1
%global nodejs_major 20
%global nodejs_minor 18
%global nodejs_patch 2
%global nodejs_abi %{nodejs_major}.%{nodejs_minor}
%global nodejs_soversion 115
%global nodejs_version %{nodejs_major}.%{nodejs_minor}.%{nodejs_patch}
%global nodejs_release %{baserelease}
%global nodejs_datadir %{_datarootdir}/nodejs
%global v8_epoch 3
%global v8_major 11
%global v8_minor 3
%global v8_build 244
%global v8_patch 8
%global v8_abi %{v8_major}.%{v8_minor}
%global v8_version %{v8_major}.%{v8_minor}.%{v8_build}.%{v8_patch}
%global v8_release %{nodejs_epoch}.%{nodejs_major}.%{nodejs_minor}.%{nodejs_patch}.%{nodejs_release}

# c-ares - from deps/cares/include/ares_version.h
# https://github.com/nodejs/node/pull/9332
%global c_ares_version 1.33.1

# llhttp - from deps/llhttp/include/llhttp.h
%global llhttp_version 8.1.2

# libuv - from deps/uv/include/uv/version.h
%global libuv_version 1.46.0

# nghttp2 - from deps/nghttp2/lib/includes/nghttp2/nghttp2ver.h
%global nghttp2_version 1.61.0

%global icu_major 75
%global icu_minor 1
%global icu_version %{icu_major}.%{icu_minor}
%global icudatadir %{nodejs_datadir}/icudata
%{!?little_endian: %global little_endian %(%{__python3} -c "import sys;print (0 if sys.byteorder=='big' else 1)")}
# " this line just fixes syntax highlighting for vim that is confused by the above and continues literal
%global openssl_minimum 1:1.1.1
# OpenSSL3
%global openssl3_minimum 1:3.0.2
%global punycode_version 2.1.0
%global npm_epoch 1
%global npm_version 10.8.2
%global npm_release %{nodejs_epoch}.%{nodejs_major}.%{nodejs_minor}.%{nodejs_patch}.%{nodejs_release}

# uvwasi - from deps/uvwasi/include/uvwasi.h
%global uvwasi_version 0.0.21

# histogram_c - assumed from timestamps
%global histogram_version 0.9.7

# simdutf from deps/simdutf/simdutf.h
%global simdutf_version 5.5.0

# dtrace is not supported on Node.js 19+
%global dtrace_configure %{nil}

Name: nodejs
Epoch: %{nodejs_epoch}
Version: %{nodejs_version}
Release: %{nodejs_release}
Summary: JavaScript runtime
License: MIT and Apache-2.0 and ISC and BSD and AFL-2.1
Group: Development/Languages
URL: http://nodejs.org/

Source0: https://nodejs.org/dist/v%{version}/node-v%{version}.tar.xz
Source1: npmrc
Source2: btest402.js
Source3: https://github.com/unicode-org/icu/releases/download/release-%{icu_major}-%{icu_minor}/icu4c-%{icu_major}_%{icu_minor}-data-bin-l.zip
Source4: nodejs_native.attr

Patch0: 0001-Use-system-uv-zlib.patch
Patch1: CVE-2023-45853.patch
Patch2: CVE-2024-5274.patch
Patch3: CVE-2024-7971.patch

BuildRequires: python3-devel python3-setuptools  make
BuildRequires: zlib-devel python3-jinja2
BuildRequires: brotli-devel python3-unversioned-command
BuildRequires: gcc >= 8.3.0 jq
BuildRequires: gcc-c++ >= 8.3.0 unzip
BuildRequires: nodejs-packaging
BuildRequires: chrpath
BuildRequires: ninja-build
BuildRequires: libatomic
BuildRequires: systemtap-sdt-devel
BuildRequires: libuv-devel >= 1:%{libuv_version}
Requires: libuv >= 1:%{libuv_version}

Provides: bundled(nghttp2) = %{nghttp2_version}
Provides: bundled(llhttp) = %{llhttp_version}
Provides: bundled(simdutf) = %{simdutf_version}

BuildRequires: openssl-devel >= %{openssl_minimum}
Requires: openssl >= %{openssl_minimum}
Requires: ca-certificates
Requires: nodejs-libs%{?_isa} = %{nodejs_epoch}:%{version}-%{release}
Recommends: nodejs-full-i18n%{?_isa} = %{nodejs_epoch}:%{version}-%{release}
Provides: nodejs(abi) = %{nodejs_abi}
Provides: nodejs(abi%{nodejs_major}) = %{nodejs_abi}
Provides: nodejs(v8-abi) = %{v8_abi}
Provides: nodejs(v8-abi%{v8_major}) = %{v8_abi}
Provides: nodejs(engine) = %{nodejs_version}
Conflicts: node <= 0.3.2-12
Provides: nodejs-punycode = %{punycode_version}
Provides: npm(punycode) = %{punycode_version}
Provides: bundled(c-ares) = %{c_ares_version}
Provides: bundled(v8) = %{v8_version}
Provides: bundled(icu) = %{icu_version}
Provides: bundled(uvwasi) = %{uvwasi_version}
Provides: bundled(histogram) = %{histogram_version}
Provides: bundled(ada) = 2.7.4
Requires: (nodejs-packaging if rpm-build)
Recommends: npm >= %{npm_epoch}:%{npm_version}-%{npm_release}%{?dist}

%description
Node.js is a platform built on Chrome's JavaScript runtime
for easily building fast, scalable network applications.
Node.js uses an event-driven, non-blocking I/O model that
makes it lightweight and efficient, perfect for data-intensive
real-time applications that run across distributed devices.

%package devel
Summary: JavaScript runtime - development headers
Group: Development/Languages
Requires: %{name}%{?_isa} = %{epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}
Requires: openssl-devel%{?_isa}
Requires: zlib-devel%{?_isa}
Requires: brotli-devel%{?_isa}
Requires: nodejs-packaging
Requires: libuv-devel%{?_isa}

%description devel
Development headers for the Node.js JavaScript runtime.

%package libs
Summary: Node.js and v8 libraries

%if 0%{?__isa_bits} == 64
Provides: libv8.so.%{v8_major}()(64bit)
Provides: libv8_libbase.so.%{v8_major}()(64bit)
Provides: libv8_libplatform.so.%{v8_major}()(64bit)
%else
Provides: libv8.so.%{v8_major}
Provides: libv8_libbase.so.%{v8_major}
Provides: libv8_libplatform.so.%{v8_major}
%endif

Provides: v8 = %{v8_epoch}:%{v8_version}-%{nodejs_release}%{?dist}
Provides: v8%{?_isa} = %{v8_epoch}:%{v8_version}-%{nodejs_release}%{?dist}
Obsoletes: v8 < 1:6.7.17-10

%description libs
Libraries to support Node.js and provide stable v8 interfaces.

%package full-i18n
Summary: Non-English locale data for Node.js
Requires: %{name}%{?_isa} = %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}

%description full-i18n
Optional data files to provide full-icu support for Node.js. Remove this
package to save space if non-English locales are not needed.

%package -n v8-devel
Summary: v8 - development headers
Epoch: %{v8_epoch}
Version: %{v8_version}
Release: %{v8_release}
Requires: %{name}-devel%{?_isa} = %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}

%description -n v8-devel
Development headers for the v8 runtime.

%package -n npm
Summary: Node.js Package Manager
Epoch: %{npm_epoch}
Version: %{npm_version}
Release: %{npm_release}

Obsoletes: npm < 1:9
Provides: npm = %{npm_epoch}:%{npm_version}
Requires: nodejs = %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}
Recommends: nodejs-docs = %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}
Provides: npm(npm) = %{npm_version}

%description -n npm
npm is a package manager for node.js. You can use it to install and publish
your node programs. It manages dependencies and does other cool stuff.

%package docs
Summary: Node.js API documentation
Group: Documentation
BuildArch: noarch

Conflicts: %{name} > %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}
Conflicts: %{name} < %{nodejs_epoch}:%{nodejs_version}-%{nodejs_release}%{?dist}

%description docs
The API documentation for the Node.js JavaScript runtime.

%prep
%autosetup -p1 -n node-v%{nodejs_version}
rm -rf deps/zlib
rm -rf deps/brotli
rm -rf deps/v8/third_party/jinja2
rm -rf tools/inspector_protocol/jinja2
pathfix.py -i %{__python3} -pn $(find -type f ! -name "*.js")
find . -type f -exec sed -i "s~/usr\/bin\/env python~/usr/bin/python3~" {} \;
find . -type f -exec sed -i "s~/usr\/bin\/python\W~/usr/bin/python3~" {} \;
sed -i "s~usr\/bin\/python2~usr\/bin\/python3~" ./deps/v8/tools/gen-inlining-tests.py
sed -i "s~usr\/bin\/python.*$~usr\/bin\/python3~" ./deps/v8/tools/mb/mb_test.py
find . -type f -exec sed -i "s~python -c~python3 -c~" {} \;

%build
%global optflags %(echo %{optflags} | sed 's/-g /-g1 /')

export CC='%{__cc}'
export CXX='%{__cxx}'
export CFLAGS='%{optflags} \
               -D_LARGEFILE_SOURCE \
               -D_FILE_OFFSET_BITS=64 \
               -DZLIB_CONST \
               -fno-delete-null-pointer-checks \
               -O3 \
               -fno-ipa-icf'
export CXXFLAGS='%{optflags} \
                 -D_LARGEFILE_SOURCE \
                 -D_FILE_OFFSET_BITS=64 \
                 -DZLIB_CONST \
                 -fno-delete-null-pointer-checks \
                 -O3 \
                 -fno-ipa-icf'

export CFLAGS="$(echo ${CFLAGS} | tr -s '[:space:]\\' ' ')"
export CXXFLAGS="$(echo ${CXXFLAGS} | tr -s '[:space:]\\' ' ')"
export LDFLAGS="%{build_ldflags}"

%if "%toolchain" == "clang"
export CFLAGS="$(echo ${CFLAGS} | sed -e s/-fno-ipa-icf//g)"
export CXXFLAGS="$(echo ${CXXFLAGS} | sed -e s/-fno-ipa-icf//g)"
%endif

%{__python3} configure.py --prefix=%{_prefix} \
           --verbose \
           --ninja \
           --shared \
           --libdir=%{_lib} \
           --shared-openssl \
           --shared-zlib \
           --shared-brotli \
           --shared-libuv \
           %{dtrace_configure} \
           --with-intl=small-icu \
           --with-icu-default-data-dir=%{icudatadir} \
           --without-corepack \
           --openssl-use-def-ca-store \
%if "%toolchain" != "clang"
           --enable-lto
%endif

%ninja_build -C out/Release

%install
# The ninja build does not put the shared library in the expected location, so
# we will move it.
mv out/Release/lib/libnode.so.%{nodejs_soversion} out/Release/

%if 0%{?nodejs_major} >= 20
./tools/install.py install --dest-dir %{buildroot} --prefix %{_prefix}
%else
./tools/install.py install %{buildroot} %{_prefix}
%endif

chmod 0755 %{buildroot}/%{_bindir}/node
chrpath --delete %{buildroot}%{_bindir}/node
ln -s libnode.so.%{nodejs_soversion} %{buildroot}%{_libdir}/libnode.so

for header in %{buildroot}%{_includedir}/node/libplatform %{buildroot}%{_includedir}/node/v8*.h; do
    header=$(basename ${header})
    ln -s ./node/${header} %{buildroot}%{_includedir}/${header}
done
ln -s ./node/cppgc %{buildroot}%{_includedir}/cppgc
for soname in libv8 libv8_libbase libv8_libplatform; do
    ln -s libnode.so.%{nodejs_soversion} %{buildroot}%{_libdir}/${soname}.so
    ln -s libnode.so.%{nodejs_soversion} %{buildroot}%{_libdir}/${soname}.so.%{v8_major}
done

mkdir -p %{buildroot}%{_prefix}/lib/node_modules

install -Dpm0644 %{SOURCE4} %{buildroot}%{_rpmconfigdir}/fileattrs/nodejs_native.attr
cat << EOF > %{buildroot}%{_rpmconfigdir}/nodejs_native.req
#!/bin/sh
echo 'nodejs(abi%{nodejs_major}) >= %nodejs_abi'
echo 'nodejs(v8-abi%{v8_major}) >= %v8_abi'
EOF
chmod 0755 %{buildroot}%{_rpmconfigdir}/nodejs_native.req

mkdir -p %{buildroot}%{_pkgdocdir}/html
cp -pr doc/* %{buildroot}%{_pkgdocdir}/html
rm -f %{buildroot}%{_pkgdocdir}/html/nodejs.1

mkdir -p %{buildroot}%{_datadir}/node
cp -p common.gypi %{buildroot}%{_datadir}/node

mv %{buildroot}/%{_datadir}/doc/node/gdbinit %{buildroot}/%{_pkgdocdir}/gdbinit

mkdir -p %{buildroot}%{_mandir} \
         %{buildroot}%{_pkgdocdir}/npm

cp -pr deps/npm/man/* %{buildroot}%{_mandir}/
rm -rf %{buildroot}%{_prefix}/lib/node_modules/npm/man
ln -sf %{_mandir}  %{buildroot}%{_prefix}/lib/node_modules/npm/man

cp -pr deps/npm/docs %{buildroot}%{_pkgdocdir}/npm/
rm -rf %{buildroot}%{_prefix}/lib/node_modules/npm/docs

ln -sf %{_pkgdocdir}/npm %{buildroot}%{_prefix}/lib/node_modules/npm/docs

rm -f %{buildroot}/%{_defaultdocdir}/node/lldb_commands.py \
      %{buildroot}/%{_defaultdocdir}/node/lldbinit

find %{buildroot}%{_prefix}/lib/node_modules/npm \
    -not -path "%{buildroot}%{_prefix}/lib/node_modules/npm/bin/*" \
    -executable -type f \
    -exec chmod -x {} \;

chmod 0755 %{buildroot}%{_prefix}/lib/node_modules/npm/node_modules/@npmcli/run-script/lib/node-gyp-bin/node-gyp
chmod 0755 %{buildroot}%{_prefix}/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js

mkdir -p %{buildroot}%{_sysconfdir}
cp %{SOURCE1} %{buildroot}%{_sysconfdir}/npmrc

mkdir -p %{buildroot}%{_prefix}/etc
ln -s %{_sysconfdir}/npmrc %{buildroot}%{_prefix}/etc/npmrc

mkdir -p %{buildroot}%{icudatadir}
unzip -d %{buildroot}%{icudatadir} %{SOURCE3} icudt%{icu_major}l.dat

%check
LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}/%{_bindir}/node -e "require('assert').equal(process.versions.node, '%{nodejs_version}')"
LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}/%{_bindir}/node -e "require('assert').equal(process.versions.v8.replace(/-node\.\d+$/, ''), '%{v8_version}')"
LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}/%{_bindir}/node -e "require('assert').equal(process.versions.ares.replace(/-DEV$/, ''), '%{c_ares_version}')"

LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}/%{_bindir}/node -e "require(\"assert\").equal(require(\"punycode\").version, '%{punycode_version}')"

LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}%{_bindir}/node %{buildroot}%{_bindir}/npm version --json |jq -e '.npm == "%{npm_version}"'

NODE_PATH=%{buildroot}%{_prefix}/lib/node_modules:%{buildroot}%{_prefix}/lib/node_modules/npm/node_modules LD_LIBRARY_PATH=%{buildroot}%{_libdir} %{buildroot}/%{_bindir}/node --icu-data-dir=%{buildroot}%{icudatadir} %{SOURCE2}


%files
%{_bindir}/node
%dir %{_prefix}/lib/node_modules
%dir %{_datadir}/node


%{_rpmconfigdir}/fileattrs/nodejs_native.attr
%{_rpmconfigdir}/nodejs_native.req
%doc CHANGELOG.md onboarding.md GOVERNANCE.md README.md
%doc %{_mandir}/man1/node.1*

%files devel
%{_includedir}/node
%{_libdir}/libnode.so
%{_datadir}/node/common.gypi
%{_pkgdocdir}/gdbinit

%files full-i18n
%dir %{icudatadir}
%{icudatadir}/icudt%{icu_major}*.dat

%files libs
%license LICENSE
%{_libdir}/libnode.so.%{nodejs_soversion}
%{_libdir}/libv8.so.%{v8_major}
%{_libdir}/libv8_libbase.so.%{v8_major}
%{_libdir}/libv8_libplatform.so.%{v8_major}
%dir %{nodejs_datadir}/

%files -n v8-devel
%{_includedir}/libplatform
%{_includedir}/v8*.h
%{_libdir}/libv8.so
%{_includedir}/cppgc
%{_libdir}/libv8_libbase.so
%{_libdir}/libv8_libplatform.so

%files -n npm
%{_bindir}/npm
%{_bindir}/npx
%{_prefix}/lib/node_modules/npm
%config(noreplace) %{_sysconfdir}/npmrc
%{_prefix}/etc/npmrc
%ghost %{_sysconfdir}/npmignore
%doc %{_mandir}/man*/
%exclude %doc %{_mandir}/man1/node.1*


%files docs
%dir %{_pkgdocdir}
%doc doc
%{_pkgdocdir}/html
%{_pkgdocdir}/npm/docs

%changelog
* Thu Jan 23 2025 wangkai <13474090681@163.com> - 1:20.18.2-1
- Update to 20.18.2
- Fix CVE-2025-23083 CVE-2025-23085 CVE-2025-23084 CVE-2024-36137
  CVE-2024-22018 CVE-2024-22020

* Mon Jan 06 2025 Ge Wang <wang__ge@126.com> - 1:20.12.1-3
- CVE-2023-45853 - Reject overflows of zip header fields in minizip
- CVE-2024-5274 - Using FunctionParsingScope for parsing class static
- CVE-2024-5535 - Fix SSL_select_next_proto
- CVE-2024-7971 - Spill all loop inputs before entering loop

* Fri Nov 29 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 1:20.12.1-2
- fix building w/ clang: only apply '-fno-ipa-icf' for gcc && disable LTO for clang + ld.bfd
- improve the handling of trilling whitespaces in C/CXXFLAGS

* Wed Sep 18 2024 yaoxin <yao_xin001@hoperun.com> - 1:20.12.1-1
- Update to 20.12.1:
  * CVE-2024-27983 - Assertion failed in node::http2::Http2Session::~Http2Session()
    leads to HTTP/2 server crash- (High)
  * CVE-2024-27982 - HTTP Request Smuggling via Content Length Obfuscation - (Medium)
  * llhttp version 9.2.1
  * undici version 5.28.4

* Mon May 06 2024 Ge Wang <wang__ge@126.com> - 1:20.11.1-3
- Fix fd is null when calling clearBuffer

* Mon Mar 18 2024 Eustace <eusteuc@outlook.com> - 1:20.11.1-2
- Revert some v8 roll

* Mon Feb 19 2024 wangkai <13474090681@163.com> - 1:20.11.1-1
- Update to 20.11.1
- Fix CVE-2023-46809 CVE-2024-21896 CVE-2024-22019 CVE-2024-21892 CVE-2024-24758 CVE-2024-22025

* Mon Nov 27 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 1:20.10.0-1
- Update to the new LTS version 20.10.0
- Add CFLAGS "-O3 -fno-ipa-icf"
- Enable lto

* Thu Aug 31 2023 Funda Wang <fundawang@yeah.net> - 1:18.17.1-1
- Update to 18.17.1
- Use huaweicloud.com mirror as default registry

* Thu May 18 2023 misaka00251 <liuxin@iscas.ac.cn> - 1:18.16.0-1
- Update to 18.16.0

* Sat Mar 11 2023 Tom_zc <tom_toworld@163.com> - 1:16.15.0-3
- support openssl v3.0.8

* Thu Feb 23 2023 yaoxin <yaoxin30@h-partners.com> - 1:16.15.0-2
- Fix CVE-2023-0286,CVE-2023-0215,CVE-2022-4304 and CVE-2022-4450

* Fri Feb 10 2023 liyanan <liyanan32@h-partners.com> - 1:16.15.0-1
- Update to 16.15.0

* Fri Feb 03 2023 yaoxin <yaoxin30@h-partners.com> - 1:12.22.11-5
- Fix build failed due to openssl update to 3.0

* Wed Nov 16 2022 liyuxiang <liyuxiang@ncti-gba.cn> - 1:12.22.11-4
- fix CVE-2022-43548

* Wed Aug 03 2022 wangkai <wangkai385@h-partners.com> - 1:12.22.11-3
- Rollback remove dist

* Tue Aug 02 2022 wulei <wulei80@huawei.com> - 1:12.22.11-2
- Dist is controlled by the version project, dist is deleted

* Mon Jun 06 2022 wangkai <wangkai385@h-partners.com> - 1:12.22.11-1
- Update to 12.22.11

* Fri Apr 15 2022 liyanan <liyanan32@h-partners.com> - 1:12.18.4-8
- The third party software jinja2-support python 3.10.0

* Thu Oct 21 2021 yaoxin <yaoxin30@huawei.com> - 1:12.18.4-7
- fix CVE-2021-22930

* Mon Aug 16 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1:12.18.4-6
- use getauxval to fix build failure in node_main.cc

* Tue Jul 20 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1:12.18.4-5
- fix CVE-2021-22918

* Mon Mar 15 2021 xinghe <xinghe1@huawei.com> - 1:12.18.4-4
- fix CVE-2021-22883 CVE-2021-22884

* Tue Feb 09 2021 xinghe <xinghe1@huawei.com> - 1:12.18.4-3
- fix CVE-2020-8265 CVE-2020-8287

* Sat Jan 04 2020 huanghaitao <huanghaitao8@huawei.com> - 1:12.18.4-2
- Make AARCH64 compile on 64KB physical pages to fix build error

* Wed Nov 18 2020 lingsheng <lingsheng@huawei.com> - 1:12.18.4-1
- Update to 12.18.4

* Tue Nov 17 2020 lingsheng <lingsheng@huawei.com> - 1:10.21.0-3
- Fix nodejs release version

* Wed Nov 04 2020 gaozhekang <gaozhekang@huawei.com> - 1:10.21.0-2
- avoid OOB read in URL parser

* Mon Aug 24 2020 lingsheng <lingsheng@huawei.com> - 1:10.21.0-1
- Update to 10.21.0

* Thu Aug 20 2020 wutao <wutao61@huawei.com> - 1:10.11.0-3
- fix dist miss problem

* Fri Mar 20 2020 shijian <shijian16@huawei.com> - 1:10.11.0-2
- Fix CVE-2018-12122 CVE-2019-5737

* Fri Mar  6 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:10.11.0-1
- Package init
